import numpy as np
import cobra

def make_irreversible_cobra(model, subset=set()):
    """Makes reactions in subset positive and irreversible."""

    if not subset:
        subset = set(model.reactions)
    else:
        subset = set(subset)

    new_reactions = []

    for r in list(subset):
        if r.lower_bound < 0:
            if r.upper_bound > 0:
                # Create new reverse reaction
                r_rev = cobra.Reaction(r.id + "_rev")
                r_rev.add_metabolites({m: -c for m, c in r.metabolites.items()})
                r_rev.bounds = np.max([0, -r.upper_bound]), -r.lower_bound
                for gene in r.genes:
                    r_rev._associate_gene(gene)
                new_reactions.append(r_rev)
                subset.add(r_rev)
            else:
                # Reverse existing reaction
                r.bounds = np.max([0, -r.upper_bound]), -r.lower_bound
                r.add_metabolites({m: -2 * c for m, c in r.metabolites.items()})
                model.remove_reactions([r])
                r.id += "_rev"
                new_reactions.append(r)
        if r.upper_bound > 0:
            # Make existing reaction irreversible
            r.lower_bound = np.max([0, r.lower_bound])

    model.add_reactions(new_reactions)
    model.repair()
    return subset
import cobra
from utils import make_irreversible_cobra
from scipy.optimize import minimize
import numpy as np
from collections import defaultdict
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
import seaborn as sns
constr_map = {"sum_of_fluxes": 0, "ATPM": 1}
import optlang
import copy
LARGE = 1e9

class GAEvalWrapper:
    def __init__(self):
        model_dict, data_dict = get_model_and_data_dict()
        self.model_dict = model_dict
        self.data_dict = data_dict

    @staticmethod
    def l_s_ga_wrap(params, self_arg=None):
        ss = least_squares_eval(params, self_arg.data_dict, self_arg.model_dict)
        return np.array([ss, np.sum(params)])

def plot_opt_surface(data_dict, model_dict):
    points = 20
    x = np.outer(np.linspace(1, 1.5, points), np.ones(points))
    y = np.outer(np.linspace(0, 10, points), np.ones(points)).T
    z = pd.DataFrame(0, index=x[:,0], columns=y[0,:])
    for x_ind in range(points):
        for y_ind in range(points):
            z.loc[x[x_ind, y_ind], y[x_ind, y_ind]] = least_squares_eval([x[x_ind, y_ind], y[x_ind, y_ind]], data_dict, model_dict)
    z[z.values >= LARGE] = -1
    sns.heatmap(np.log(z), cmap="YlGnBu", mask=np.isnan(z))
    plt.savefig("landscape.pdf")

def least_squares_eval(in_params, data_dict, model_dict):
    params = np.squeeze(in_params)
    # reg = 1e-5
    # sum of squares (ss) starts at zero, ignore regularization for now
    ss = 0 #np.sum(np.square(params))*reg
    for model, constraints in model_dict.items():
        sol = model.optimize()
        # check if the problem is still feasible
        assert sol.status == "optimal"
        orig_bounds = dict()
        for c_name, constraint in constraints.items():
            orig_bounds[c_name] = constraint.ub
            # impose the new upper bounds suggested by in_params (q)
            constraint.ub = orig_bounds[c_name]*params[constr_map[c_name]]
        model.repair()
        #IMPORTANT STEP now solve the inner problem with the imposed bounds
        sol = model.optimize()
        # constrain uptake to minimum
        if sol.status == 'infeasible':
            # ss = np.nan
            ss += LARGE
            for c_name, constraint in constraints.items():
                constraint.ub = orig_bounds[c_name]
            model.repair()
            break
        # Force the model to obey the solution obtained from the inner problem
        input_reaction = model.reactions.get_by_id(model.id)
        input_reaction.bounds = (-sol.objective_value, -sol.objective_value)
        model.repair()
        # MOMA (supposedly) solves for the closest feasible solution and gives us the sum of squares residual
        moma = cobra.flux_analysis.moma(model, data_dict[model.id], linear=False)
        # now relax the bound from the solution obtained solution of the inner problem
        input_reaction.bounds = (-100, 1000)
        # add the difference to the sum of fluxes
        ss += moma.objective_value
        # reset the bounds (q:s) to the default
        for c_name, constraint in constraints.items():
            constraint.ub = orig_bounds[c_name]
        model.repair()
    return ss


def make_models(model, react_names_growth, constraints):
    # for each (of the two) models:
    model_dict = dict()
    for react_name, rate in react_names_growth.items():
        copy = model.copy()

        # enforce a growth rate (can be made as a measurement instead)
        copy.reactions.BIOMASS_Ecoli_core_w_GAM.bounds = (rate, rate)
        # relax the bound on the desired uptake reaction
        copy.reactions.get_by_id(react_name).lower_bound = -100
        # make sure the bound the undesired reaction is zero
        for not_react_name in react_names_growth:
            if not not_react_name == react_name:
                copy.reactions.get_by_id(not_react_name).lower_bound = 0
        # make the objective minimization of the uptake (glucose or pyruvate)
        copy.objective = {copy.reactions.get_by_id(react_name): -1}
        copy.objective.direction = "min"
        copy.id = react_name
        new_constraints = dict()
        for c_name in constraints:
            if c_name == "sum_of_fluxes":
                # make a dictionary of all intracellular reactions
                reaction_dict = {r: 1 for r in list(set(copy.reactions) - set(copy.exchanges))}
                # impose a row in Q (can be made in a smarter way)
                new_constraints[c_name] = impose_sum_of_fluxes_constraint(copy, reaction_dict, name=c_name)
            if c_name == "ATPM":
                # make a corresponding row in Q for ATPM
                atpm = copy.reactions.get_by_id(c_name)
                atpm.lower_bound = 0
                constraint = copy.problem.Constraint(
                    -atpm.flux_expression,
                    name=c_name,
                    lb=None,
                    ub=-1)
                copy.add_cons_vars(constraint)
                new_constraints[c_name] = constraint
                copy.repair()
        # pack the model in dictionary, labelled after its input flux
        model_dict[copy] = new_constraints
    return model_dict


def make_data(model_dict, true_alphas):
    # dictionary to hold simulation results
    sol_dict = dict()
    # make sure that the maximum upper bound across conditions is rused for all models (we cannot have different bounds)
    min_max_dict = defaultdict(list)
    for constraints in model_dict.values():
        for name, constr in constraints.items():
            min_max_dict[name].append(constr.ub)
    for name, v_list in min_max_dict.items():
        min_max_dict[name] = np.max(v_list)


    for model, constraints in model_dict.items():
        orig_bounds = dict()
        # for each row in Q, insert the true q as bound. Here, alpha is given in relation to the optimum for sum of fluxes
        for c_name, constraint in constraints.items():
            upper_bound = min_max_dict[c_name] * true_alphas[c_name]
            constraint.ub = upper_bound

        model.repair()
        sol = model.optimize()
        # test that the ground truth data does not violate the imposed sum of fluxes constraint
        restricted_reactions = [r.id for r in list(set(model.reactions) - set(model.exchanges))]
        sum = sol.fluxes[restricted_reactions].abs().sum()
        assert sum <= constraints["sum_of_fluxes"].ub + 1e-6
        # now reinstate the original bounds min_max bounds
        for c_name, constraint in constraints.items():
            constraint.ub = min_max_dict[c_name]
        model.repair()
        sol_dict[model.id] = sol
    return sol_dict


def impose_sum_of_fluxes_constraint(model, reaction_dict, name="sum_of_fluxes"):

    coefficients = dict()
    for r, val in reaction_dict.items():
        coefficients[r.forward_variable] = val
        coefficients[r.reverse_variable] = val

    orig_obj = copy.deepcopy(model.objective)
    model.solver.objective = model.problem.Objective(optlang.symbolics.Zero, "min")
    model.solver.objective.set_linear_coefficients(coefficients)
    sol = model.optimize("minimize")
    constr_opt = sol.objective_value
    model.objective = orig_obj
    model.repair()
    # impose constraint
    coefficients = dict()
    for r, val in reaction_dict.items():
        coefficients[r.forward_variable] = val
        coefficients[r.reverse_variable] = val
    upper_bound = constr_opt
    constraint = model.problem.Constraint(
        0,
        name=name,
        lb=None,
        ub=upper_bound)
    model.add_cons_vars(constraint)
    model.solver.update()
    constraint.set_linear_coefficients(coefficients=coefficients)
    model.repair()
    return constraint




def get_model_and_data_dict(constraints):
    # read in the model
    model = cobra.io.read_sbml_model("models/e_coli_core.xml")
    # set the maximal growth rate  for the two considered scenarios, growth on glucose and pyruvate. The funny identifiers
    # correspond to the names of the input fluxes of glucaose and pyruvate
    react_names_growth = {"EX_glc__D_e":0.6, "EX_pyr_e":0.55}

    # construct the corresponding models (This is a design choice, instead of one model per condition, you could have one model and vary the uptake constraints)
    model_dict = make_models(model, react_names_growth, constraints.index)
    # generate synthetic data
    data_dict = make_data(model_dict, constraints)
    return model_dict, data_dict

def main():
    # specify the names of the constraints and their "true" q values
    constraints = pd.Series({"sum_of_fluxes": 1.05, "ATPM": 8.39})
    # read in the models and produce synthetic data corresponding to the values above
    model_dict, data_dict = get_model_and_data_dict(constraints)

    # check twice (do we disturb the inner state?) whether we can get 0 ss at the true solution
    for i in range(2):
        ss = least_squares_eval(constraints.values, data_dict, model_dict)
        assert ss < 1e-10
    #check sensitivity by testing some other values
    ss1 = least_squares_eval([1, 8.39], data_dict, model_dict)
    assert ss1 > 1
    ss2 = least_squares_eval([5, 8.39], data_dict, model_dict)
    assert ss2 > 1
    ss3 = least_squares_eval([1.05, 5], data_dict, model_dict)
    assert ss3 > 1
    # what does the optimization landscape look like?
    plot_opt_surface(data_dict, model_dict)
    # try to solve the problem from a non perfect starting point
    start_x = [1.045, 5]
    bounds = [(1, None)]*len(start_x)
    res = minimize(least_squares_eval, start_x, args=(data_dict, model_dict), bounds=bounds)


    pass




if __name__ == "__main__":
    main()